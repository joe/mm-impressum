# Copyright (C) 2018 by Jorrit Fahlke
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.




"""Override definition of MailmanLogo

This overrides the definition of MailmanLogo() from Mailman.htmlformat to add
some links to all page footers.
"""

# load the original diverted module in-place
execfile("/usr/lib/mailman/Mailman/htmlformat.mailman.py")

# stow away the original function for internal use
_origMailmanLogo = MailmanLogo

def MailmanLogo():
    from Mailman import mm_cfg

    logo = _origMailmanLogo()

    footer = None
    if hasattr(mm_cfg, "WEB_FOOTER_ADD"):
        footer = mm_cfg.WEB_FOOTER_ADD

    if footer is None:
        return logo
    else:
        return Container(logo, footer)

